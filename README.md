# GitLab CI template for Robot Framework

This project implements a GitLab CI/CD template to run your automated tests with [Robot Framework](https://robotframework.org/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/robotframework/gitlab-ci-robotframework@4.3.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      tests-dir: "e2e"
      review-enabled: "true"
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/robotframework'
    ref: '4.3.0'
    file: '/templates/gitlab-ci-robotframework.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  ROBOT_TESTS_DIR: "e2e"
  REVIEW_ENABLED: "true"
```

## Jobs

### `robotframework-lint` job

This job performs a **lint** analysis on Robot Framework files, using [robotframework-lint](https://github.com/boakley/robotframework-lint/).

It is bound to the `test` stage, and uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `lint-disabled` / `ROBOT_LINT_DISABLED` | Set to `true` to disable linter                           | _none_ |

### `robotframework` job

This job performs [Robot Framework tests](https://robotframework.org/).

It is bound to the `acceptance` stage, and uses the following variable:

| Input / Variable | Description                                                 | Default value |
| --------------------- | ----------------------------------------------------------- | ------------- |
| `base-image` / `ROBOT_BASE_IMAGE` | The Docker image used to run Robot Framework                | `registry.hub.docker.com/ppodgorsek/robot-framework:latest` |
| `tests-dir` / `ROBOT_TESTS_DIR` | Robot Framework's tests directory                           | `robot` |
| `threads` / `ROBOT_THREADS` | Number of threads to execute Robot Framework's tests (uses [Pabot](https://pabot.org/) if > `1`)  | `1` |
| `screen-colour-depth` / `ROBOT_SCREEN_COLOUR_DEPTH` | Screen colour depth for X Window Virtual Framebuffer  | `24`   |
| `screen-height` / `ROBOT_SCREEN_HEIGHT` | Screen height for X Window Virtual Framebuffer              | `1080` |
| `screen-width` / `ROBOT_SCREEN_WIDTH` | Screen width for X Window Virtual Framebuffer               | `1920` |
| `browser` / `ROBOT_BROWSER` | Browser to use (one of `firefox` or `chrome`)               | `firefox` |
| `options` / `ROBOT_OPTIONS` | Robot Framework [additional options](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options) | _none_ |
| `pabot-options` / `PABOT_OPTIONS` | Pabot [additional options](https://github.com/mkorpela/pabot#command-line-options) (if `ROBOT_THREADS` > `1`)   | _none_ |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Robot Framework tests on review environments (_dynamic environments instantiated on development branches_) | _none_ (disabled) |

Notes:
* :warning: the Robot Framework project does not maintain an official Docker image. Use the default Docker image at your own risks. See the to-be-continuous [documentation](https://to-be-continuous.gitlab.io/doc/secu/#image-selection) for more details.
* :warning: the default Docker image used here runs as non-`root` user. Therefore it is not able - like most other templates - to manage
custom certificate authorities (declared using either `$CUSTOM_CA_CERTS` or `$DEFAULT_CA_CERTS`). If you need it (need to call a server
with SSL certificate issued by a non-official certificate authority), then you'll have to build your own Docker image that either embeds
the required CA certificates, or that runs as `root` (thus the template will be able to install custom certificate authorities).
* The template runs [XVFB](https://www.x.org/releases/X11R7.6/doc/man/man1/Xvfb.1.xhtml) with `-screen` parameter
  `$ROBOT_SCREEN_WIDTHx$ROBOT_SCREEN_HEIGHTx$ROBOT_SCREEN_COLOUR_DEPTH` to run tests.
* Depending on the `ROBOT_THREADS` value, the template runs either [`robot`](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options)
  or [`pabot`](https://pabot.org/) (if `ROBOT_THREADS` > 1).
* Tests are run automatically on `master` and `develop` branches, and manually on others (may be overridden setting the
  `REVIEW_ENABLED` variable).

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/robotframework.xunit.xml` | [xUnit](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#xunit-compatible-result-file) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### Robot Framework `{{ BASE_URL }}` auto evaluation

By default, the Robot Framework template tries to auto-evaluate the [`{{ BASE_URL }}` variable](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#variables)
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Robot Framework test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Robot Framework tests.

### Hook scripts

The Robot Framework template supports _optional_ **hook scripts** from your project, located in the `$ROBOT_TESTS_DIR` directory to perform additional project-specific logic:

* `pre-robot.sh` is executed **before** running Robot Framework,
* `post-robot.sh` is executed **after** running Robot Framework (whichever the tests status).

### How to manage Robot Framework tests in a separate GitLab repository

This template can obviously be used in an application repository that also embeds the Robot Framework test scripts.

But you may also decide to manage your acceptance tests in a **separate GitLab repository** (for instance if people developing those tests have
their own project cycle, separate from the application itself).

On the repository containing the Robot Framework tests, you should create a Docker image containing Robot Framework + your test scripts.

`Dockerfile` example:

```Dockerfile
FROM ppodgorsek/robot-framework:latest

LABEL name="My acceptance tests"
LABEL description="Robot Framework + tests in Docker"
LABEL url="https://gitlab.acme.host/my-project/rf-tests"
LABEL maintainer="john.dev@acme.com"

COPY robotframework/tests /opt/robotframework/tests

RUN pip3 install --no-cache-dir \
    <some robot framework library>
```

`.gitlab-ci.yml` example (build the Docker image using the Docker template):

```yml
include:
  # Include Docker template (to build the Docker image)
  - project: 'to-be-continuous/docker'
    ref: '<tag>'
    file: '/templates/gitlab-ci-docker.yml'
  # Include Robot Framework (to run robotframework-lint)
  - project: 'to-be-continuous/robotframework'
    ref: '4.3.0'
    file: '/templates/gitlab-ci-robotframework.yml'
```

And lastly in the repository containing the application to test, you'll have to use the Robot Framework template but override
`ROBOT_BASE_IMAGE` with your own image built above.

```yml
include:
  # Include Robot Framework
  - project: 'to-be-continuous/robotframework'
    ref: '4.3.0'
    file: '/templates/gitlab-ci-robotframework.yml'

variables:
  # use my own Robot Framework image (with embedded tests)
  ROBOT_IMAGE: '$CI_REGISTRY/my-project/rf-tests:master'
  ROBOT_THREADS: 4
  # Important: override the tests directory
  ROBOT_TESTS_DIR: "/opt/robotframework/tests"
```

## Variants

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url          | _none_ |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

> :information_source: 
>
> `VAULT_ROLE_ID` and `VAULT_SECRET_ID` can be replaced by `VAULT_JWT_ROLE` and `VAULT_JWT_TOKEN`. More informations can be found [HERE](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/robotframework/gitlab-ci-robotframework@4.3.0
  # Vault variant
  - component: gitlab.com/to-be-continuous/robotframework/gitlab-ci-robotframework-vault@4.3.0
    inputs:
      # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"
      # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable

variables:
  ### Secrets managed by Vault
  MY_APPLICATION_PASS: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/robot/noprod?field=application_password"
```
