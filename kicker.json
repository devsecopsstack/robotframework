{
  "name": "Robot Framework",
  "description": "Run your automated tests with [Robot Framework](https://robotframework.org/)",
  "template_path": "templates/gitlab-ci-robotframework.yml",
  "kind": "acceptance",
  "prefix": "robot",
  "job_prefix": "robotframework",
  "is_component": true,
  "variables": [
    {
      "name": "ROBOT_BASE_IMAGE",
      "description": "The Docker image used to run Robot frame work CLI",
      "default": "registry.hub.docker.com/ppodgorsek/robot-framework:latest"
    },
    {
      "name": "ROBOT_TESTS_DIR",
      "description": "Path to Robot Framework tests directory",
      "default": "robot"
    },
    {
      "name": "ROBOT_BROWSER",
      "description": "Browser to use",
      "default": "firefox",
      "type": "enum",
      "values": ["firefox", "chrome"]
    },
    {
      "name": "ROBOT_OPTIONS",
      "description": "Robot Framework [additional options](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options)",
      "advanced": true
    },
    {
      "name": "ROBOT_THREADS",
      "description": "Number of threads to execute Robot Framework tests (uses [Pabot](https://pabot.org/) if > `1`)",
      "default": "1",
      "type": "number",
      "advanced": true
    },
    {
      "name": "PABOT_OPTIONS",
      "description": "Pabot [additional options](https://github.com/mkorpela/pabot#command-line-options) (if `ROBOT_THREADS` > `1`)",
      "advanced": true
    },
    {
      "name": "ROBOT_SCREEN_COLOUR_DEPTH",
      "description": "Screen colour depth for X Window Virtual Framebuffer",
      "type": "number",
      "default": "24"
    },
    {
      "name": "ROBOT_SCREEN_HEIGHT",
      "description": "Screen height for X Window Virtual Framebuffer",
      "type": "number",
      "default": "1080"
    },
    {
      "name": "ROBOT_SCREEN_WIDTH",
      "description": "Screen width for X Window Virtual Framebuffer",
      "type": "number",
      "default": "1920"
    },
    {
      "name": "REVIEW_ENABLED",
      "description": "Set to enable Robot Framework tests on review environments (dynamic environments instantiated on development branches)",
      "type": "boolean"
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "robotframework-lint",
      "description": "This job performs a [Lint](https://github.com/boakley/robotframework-lint/) analysis on your `Robot Framework files`.",
      "disable_with": "ROBOT_LINT_DISABLED"
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-robotframework-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url"
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
